image:"mrc.microsoft.com"/dotnet/src"

before_script:
    -dotnet_resource

Stages:
    -build
    -test
    -publish

build:
   stage:build
   script:
     -"dotnet build -- no resource"

test:
  stage:test
  script:
    -"dotnet test -- no resource"

publish:
  stage:publish
  artifacts:
   -untracked:no resource
   -expiary_in:9 days
  script:
     -"dotnet resouce could not completed"
